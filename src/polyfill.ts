export const originalTextKey = '_____originalText_____';

let ispolyfill = false;
export function polyfill() {
    if (ispolyfill) return;

    Object.defineProperty(Laya.Text.prototype, 'text', {
        get: function (this: Laya.Text) {
            return this._text || "";
        },
        set: function (this: Laya.Text, value) {
            this[originalTextKey] = value;
            if (this._text !== value) {
                this.lang(value + "");
                this.isChanged = true;
                this.event(/*laya.events.Event.CHANGE*/"change");
            }
        }
    });
}

