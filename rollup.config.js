import typescript from 'rollup-plugin-typescript2';
import dts from 'rollup-plugin-dts';
import pkg from './package.json';

export default [
    {
        input: 'src/index.ts',
        output: {
            file: './dist/index.js',
            name: pkg.namespace,
            format: 'umd',
            sourcemap: true
        },
        plugins: [
            typescript({ clean: true, tsconfigOverride: { compilerOptions: { module: 'ES2015', declaration: false, declarationMap: false } } }),
        ]
    },
    {
        input: 'src/index.ts',
        output: {
            file: './dist/index.d.ts',
            name: pkg.namespace,
            format: 'es',
            footer: `export as namespace ${pkg.namespace};`
        },
        plugins: [
            typescript({ clean: true, tsconfigOverride: { compilerOptions: { module: 'ES2015', declaration: false, declarationMap: false } } }),
            dts({ respectExternal: true }),
        ]
    }

];
