import { originalTextKey, polyfill } from "./polyfill";

/**
 * 更新显示对象上
 * 
 * @param displayObject 
 */
export function updateLanguage(displayObject: laya.display.Node) {

    polyfill();

    // 查找到显示对象根对象
    let root = displayObject;
    while (root.parent) {
        root = root.parent;
    }

    // 搜索出场景树中所有的Laya.Text 
    const texts: laya.display.Text[] = searchNode(root, (node) => node instanceof laya.display.Text);
    texts.forEach(text => {
        // 在启用本地化多语言的情况下重新赋值
        if (text[originalTextKey] !== undefined) {
            text.lang(text[originalTextKey]);
            text['isChanged'] = true;
            text.event(/*laya.events.Event.CHANGE*/"change");
        } else {
            const temp = text.text;
            text.text = "";
            text.text = temp;
        }
    });
}

/**
 * 搜索场景树中所有匹配的显示对象 
 * 
 * @param root 根节点
 * @param filter 过滤器
 * @param result 搜索结果
 * @returns 搜索结果
 */
function searchNode<T>(root: laya.display.Node, filter: (node: laya.display.Node) => boolean, result: T[] = []) {
    if (!root) return result;
    if (filter(root)) {
        result.push(root as any);
    }
    if (root._childs) {
        root._childs.forEach(child => {
            searchNode(child, filter, result);
        });
    }
    return result;
}
